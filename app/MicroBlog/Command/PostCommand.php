<?PHP
namespace MicroBlog\Command;

use MicroBlog\ {
  Model\Message,
  Traits\GetClient,
  Traits\GetToken
};

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use \GuzzleHttp\Client;

/**
 * Sample code for Using the WordPress REST API
 *
 * This command posts a new command to a WordPress based MicroBlog
 */

class PostCommand extends Command
{
  use GetClient;
  use GetToken;

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [
        new InputArgument('post', InputArgument::REQUIRED,'The body of the post.'),
        new InputOption('tags', 't', InputOption::VALUE_REQUIRED, 'comma delimited list of tags to set')
      ];

      $this->setName('post')
           ->setDescription('Create a post')
           ->setDefinition($definition)
           ->setHelp("Command line tool to create a new post on a WordPress based micro-blogging platform.");
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('MicroBlogging via the WordPress API', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();
    $this->getToken($this->debug);

    $message = new Message(
      $this->getClient(),
      $this->token['token'],
      $this->getApplication()->config,
      0,
      $this->debug
    );

    $message->title = date( 'Y-m-d h:i:s');
    $message->content = $input->getArgument('post');
    $message->status =  'publish';
    $message->author =  $this->getApplication()->config['author_id'];
    $message->addTags($input->getOption('tags'));
    $message->save();

    $output->writeln('Post ID: ' . $message->id , OutputInterface::VERBOSITY_NORMAL);

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

}