<?PHP
namespace MicroBlog\Command;

use MicroBlog\Traits\GetToken;

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

/**
 * Sample code for Using the WordPress REST API
 *
 * This command validates the login credentials in the config/config.php
 */

class ValidateCommand extends Command
{
  use GetToken;

  protected $debug = false;

  protected function configure()
  {
      $definition = [];

      $this->setName('validate')
            ->setDescription('Validate Login Credentials')
            ->setDefinition($definition)
            ->setHelp("Attempts to log into the site specified with the credentials specified and either works or returns the error.");
      return;
  }


  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Validate WordPress REST API Login Credentials', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();
    $this->getToken();
    $output->writeln('Token             :' . $this->token['token'], OutputInterface::VERBOSITY_NORMAL);
    $output->writeln('User Email        :' . $this->token['user_email'], OutputInterface::VERBOSITY_VERBOSE);
    $output->writeln('User Name         :' . $this->token['user_nicename'], OutputInterface::VERBOSITY_VERBOSE);
    $output->writeln('User Display Name :' . $this->token['user_display_name'], OutputInterface::VERBOSITY_VERBOSE);
    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

}