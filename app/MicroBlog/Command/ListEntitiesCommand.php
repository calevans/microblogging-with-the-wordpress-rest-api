<?PHP
namespace MicroBlog\Command;

use MicroBlog\ {
  Model\Message,
  Model\Tag,
  Traits\GetClient,
  Traits\GetToken
};

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command,
  Helper\Table
};

use \GuzzleHttp\Client;

/**
 * Sample code for Using the WordPress REST API
 *
 * This command posts a new command to a WordPress based MicroBlog
 */

class ListEntitiesCommand extends Command
{
  use GetToken;

  protected $debug = false;
  protected $tag_cache = [];

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [
        new InputOption(
          'page',
          'p',
          InputOption::VALUE_REQUIRED,
          'The page to fetch.',
          1
        ),
        new InputOption(
          'per_page',
          'e',
          InputOption::VALUE_REQUIRED,
          'The number of messages per page to fetch.',
          10
        ),
        new InputOption(
          'filter',
          'f',
          InputOption::VALUE_REQUIRED,
          'A filter option to specify'
        )
      ];

      $this->setName('entities')
           ->setDescription('List a page of posts')
           ->setDefinition($definition)
           ->setHelp("Command line tool to list posts on a WordPress based micro-blogging platform.");
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('MicroBlogging via the WordPress API', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();
    $this->getToken($this->debug);
    $list = Message::list($this->getClient(),$this->token['token'], $this->getApplication()->config, $input->getOption('page'),$input->getOption('per_page'),[],$this->debug);

    $headers = [];
    if ($output->isVerbose()) {
      $headers[] = 'ID';
    }

    $headers[] = 'Posts';
    $headers[] = 'Tags';

    $rows = [];

    foreach ($list as $row) {
      $this_row = [];

      // ID but only if in VERBOSE mode
      if ($output->isVerbose()) {
        $this_row[] = $row->id;
      }

      // Main Content
      $this_row[] = strip_tags(trim($row->content->rendered));

      // If we have tags, process them
      $tag_names = [];

      foreach ($row->tags as $thisTag) {
        if ( ! isset( $this->tag_cache[$thisTag] ) ) {
          $this->tag_cache[$thisTag] = new Tag(
            $this->getClient(),
            $this->token['token'],
            $this->getApplication()->config,
            $thisTag,
            $this->debug
          );
        }

        $tag_names[] = $this->tag_cache[$thisTag]->name;
      }

      // TAGS
      $this_row[] = implode(',', $tag_names);

      $rows[] = $this_row;
    }

    // Now, display the table
    $table = new Table($output);
    $table->setHeaders($headers);
    $table->setRows($rows);
    $table->render();

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

}