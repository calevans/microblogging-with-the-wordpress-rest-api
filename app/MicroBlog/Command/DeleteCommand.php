<?PHP
namespace MicroBlog\Command;

use MicroBlog\ {
  Model\Message,
  Traits\GetClient,
  Traits\GetToken
};

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use \GuzzleHttp\Client;

/**
 * Sample code for Using the WordPress REST API
 *
 * This command deletes an existingpost in a WordPress based MicroBlog
 */

class DeleteCommand extends Command
{
  use GetToken;

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
    $definition = [
      new InputOption('postid', 'p', InputOption::VALUE_REQUIRED, 'The post ID to delete')
    ];

    $this->setName('delete')
         ->setDescription('Deletes an existing post')
         ->setDefinition($definition)
         ->setHelp("Command line tool to delete an existing post on a WordPress based micro-blogging platform.");
    return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('MicroBlogging via the WordPress API', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();
    $this->getToken($this->debug);

    $message = new Message(
      $this->getClient(),
      $this->token['token'],
      $this->getApplication()->config,
      $input->getOption('postid'),
      $this->debug
    );

    $message->delete();

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

}