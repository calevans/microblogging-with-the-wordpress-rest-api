<?php

namespace MicroBlog\Model;

/**
 * Base class for all WordPress entities in the system.
 */
abstract class BaseModel {
  CONST entity = '';

  protected $data = [];
  protected $token;
  protected $config = [];
  protected $debug = false;
  protected $client = null;

  /**
   * Read an entity in and store the data in the data array.
   *
   * @param int id The id of the entity to read.
   */
  public function read(int $id) {
    $response = $this->client->request(
      'GET',
      $this->config['baseurl'] . '/wp/v2/' . static::entity . '/' . $id . '/?context=edit',
      [
        'headers' => [
          'Accept'        => 'application/json',
          'Authorization' => 'Bearer ' . $this->token
          ],
        'debug' => $this->debug
      ]
    );
    $this->populateData(json_decode($response->getBody()->getContents(),true));
  }

  /**
   * Returns a list of entities from the API
   *
   * @param Client $client The Guzzle client to use
   * @param string $token The API token to use
   * @param int $page The page of entities to return
   * @param int $per_page The number of entities per page to return
   * @param array $filter A array of filter entities to add to the url
   *              e.g. 'search=dog' would result in
   *              &search=dog being added.
   * @param bool debug Debug mode
   */
  static public function list(
    \GuzzleHttp\Client $client,
    string $token,
    array $config,
    int $page = 1,
    int $per_page = 10,
    array $filter = [],
    bool $debug = false
  ) : array {
    $url = $config['baseurl'] . '/wp/v2/' . static::entity . '/?';

    if ( ! empty( $filter ) ) {
      $url .= implode('&',$filter) . '&';
    }

    $url .= 'page=' . $page . '&per_page=' . $per_page;

    $response = $client->request(
      'GET',
      $url,
      [
        'headers' => [
          'Accept'        => 'application/json',
          'Authorization' => 'Bearer ' . $token
          ],
        'debug' => $debug
      ]
    );

    return json_decode($response->getBody()->getContents());
  }

  /**
   * Write the current entity to the API
   */
  public function save() {
    $url = $this->config['baseurl'] . '/wp/v2/'. static::entity .
      ($this->data['id'] === 0 ? '' : '/' . $this->data['id']) ;

    $response = $this->client->request(
      'POST',
      $url,
      ['json' => $this->buildPayload(),
        'headers' => [
          'Authorization' => 'Bearer ' . $this->token,
          'Accept'        => 'application/json',
          'Content-type'  => 'application/json',
          ],
        'debug' => $this->debug
      ]
    );

    $this->populateData(json_decode($response->getBody()->getContents(),true));
  }

  /**
   * Delete the entity
   */
  public function delete() {
    $response = $this->client->request(
      'DELETE',
      $this->config['baseurl'] . '/wp/v2/' . static::entity . '/' . $this->data['id'],
      [
        'headers' => [
          'Accept'        => 'application/json',
          'Authorization' => 'Bearer ' . $this->token
          ],
        'debug' => $this->debug
      ]
    );
  }

  /**
   * Returns the entities data either as an array or as an object.
   *
   * @param bool $as_object If true, then return an object.
   * @return array|object
   */
  public function getData(bool $as_object = false) {
    if ( $as_object ) {
      return json_decode(json_encode($this->data));
    }
    return $this->data;
  }

  /**
   * Retrieves a value from the $data array if it exists.
   *
   * @param string $key The key to fetch
   */
  public function __get( $key ) {
    if ( ! isset ( $this->data[$key] ) ) {
      return null;
    }

    return $this->data[$key];
  }

  /**
   * Sets a value in the $data array if it exists.
   *
   * @param string $key The key to set
   * @param mixed $value The key to set
   */
  public function __set( string $key, $value ) {
    if ( ! isset ( $this->data[$key] ) ) {
      return null;
    }

    $this->data[$key] = $value;
  }

  /**
   * Updates the current object from an array
   *
   * @param array $new_data Usually from a read(), this is an array of the
   *              data to update. It will only set the values that already
   *              exist.
   */
  protected function populateData(array $new_data) {
    foreach($new_data as $key=>$value) {
      $this->$key = $value;
    }
  }

}
