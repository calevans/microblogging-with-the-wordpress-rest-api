<?php

namespace MicroBlog\Model;


class Tag extends BaseModel {
  CONST entity = 'tags';

  /**
   * Create a new Tag
   */
  public function __construct(
    \GuzzleHttp\Client $client,
    string $token,
    array $config,
    int $id = 0,
    bool $debug = false
  ) {
    $this->token  = $token;
    $this->config = $config;
    $this->debug  = $debug;
    $this->client = $client;

    $this->data = [
      'id'          => 0,
      'slug'        => '',
      'description' => '',
      'name'        => ''
    ];

    if ( $id > 0 ) {
      $this->read($id);
    }
  }

  /**
   * Creates the array that will be POSTed to the entity endpoint.
   */
  protected function buildPayload() {
    return [
      'name'        => $this->name,
      'description' => $this->description,
      'slug'        => $this->slug
    ];
  }

}