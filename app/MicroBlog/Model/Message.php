<?php

namespace MicroBlog\Model;

class Message extends BaseModel {
  CONST entity = 'posts';

  /**
   * Create a new Message
   */
  public function __construct(
    \GuzzleHttp\Client $client,
    string $token,
    array $config,
    int $id = 0,
    bool $debug = false
  ) {

    $this->token  = $token;
    $this->config = $config;
    $this->debug  = $debug;
    $this->client = $client;

    $this->data = [
      'id'      => 0,
      'title'   => '',
      'content' => '',
      'status'  => '',
      'author'  => '',
      'tags'    => []
    ];

    if ( $id>0 ) {
      $this->read($id);
    }
  }

  /**
   * Add a tag or comma delimited list of tags to the message.
   *
   * @param string|array if this is a string, it needs to be comma delimited.
   */
  public function addTags($new_tags) {
    if ( ! is_array( $new_tags ) ) {
      $new_tags = explode(',', $new_tags);
    }

    foreach( $new_tags as $tag ) {
      $tag = trim($tag);
      if ( ! in_array( $tag, $this->data['tags'] ) ) {
        $this->data['tags'][] = $tag;
      }
    }
  }

  /**
   * Add a value to the given key n the $data array if the key already exists.
   * This overrides the parent method because we handle tags differently here.
   *
   * @param string $key The key to update
   * @param mixed $value the value to update it with.
   */
  public function __set( string $key, $value ) {
    if ($key === 'tags') {
      $this->addTags($value);
      return;
    }

    if ( ! isset ( $this->data[$key] ) ) {
      return;
    }

    $this->data[$key] = $value;
  }

  /**
   * Examines the array of tags and any that are strings, it calls the API to
   * find the tag_id.
   *
   * If the tag cannot be found, it assumes this is a new tag and adds it to
   * the system.
   *
   * @return array array of tag ids
   */
  protected function resolveTags() : array {
    $return_value = [];
    foreach($this->data['tags'] as $tag) {
      if ( empty( $tag ) ) {
        continue;
      }

      if ( (int)$tag === $tag ) {
        $return_value[] = $tag;
        continue;
      }

      $tag_payload = Tag::list($this->client, $this->token, $this->config, 1,10, ['search='.$tag],$this->debug);
      if ( count( $tag_payload ) === 0 ) {
        // tag not found, create it
        $new_tag = new Tag($this->client,$this->token, $this->config, 0 ,$this->debug);
        $new_tag->name = $tag;
        $new_tag->save();
        /*
         * We wrap this in an array to be consistent with the collection we
         * got from the GET above.
         */
        $tag_payload = [$new_tag->getData(true)];
      }
      $return_value[] = $tag_payload[0]->id;
    }

    return $return_value;
  }

  /**
   * Creates the array that will be POSTed to the entity endpoint.
   */
  protected function buildPayload() {
    return [
      'title'   => $this->title,
      'content' => $this->content,
      'status'  => $this->status,
      'author'  => $this->author,
      'tags'    => $this->resolveTags()
    ];
  }

}