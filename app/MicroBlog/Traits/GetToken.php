<?php
namespace MicroBlog\Traits;

use \GuzzleHttp\Client;
use MicroBlog\Traits\GetClient;

trait GetToken {
  use GetClient;

  protected $token = '';

  protected function getToken() {
    $response = $this->getClient()->request(
      'POST',
      $this->getApplication()->config['baseurl'] . '/jwt-auth/v1/token',
      [
        'form_params' => [
          'username' => $this->getApplication()->config['credentials']['username'],
          'password' => $this->getApplication()->config['credentials']['password']
        ],
        'headers' => [
        'Accept'     => 'application/json',
        ],
        'debug' => $this->debug
      ]
    );
    $this->token = json_decode($response->getBody(), true);
  }

}
