<?php

namespace MicroBlog\Traits;

use GuzzleHttp\Client;

trait GetClient {
  protected $client;

  /**
   * Create the Guzzle Client if needed and stores it, then returns the
   * stored client.
   */
  protected function getClient() : Client {
    if (is_null($this->client)) {
      $this->client = new Client();
    }

    return $this->client;
  }
}
