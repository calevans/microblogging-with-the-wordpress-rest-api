#!/usr/bin/env php
<?php
require_once __DIR__.'/../vendor/autoload.php';

use MicroBlog\Command;
use Symfony\Component\Console\Application;

$app_path = realpath(dirname(__FILE__) . '/../') . '/';

$config = include $app_path . "/config/config.php";
$app = new Application('MicroBlogger', '1.0.0');
$app->config = $config;

$app->addCommands(
  [
    new Command\ValidateCommand(),
    new Command\PostCommand(),
    new Command\ListEntitiesCommand(),
    new Command\DeleteCommand(),
  ]
);

$app->run();
