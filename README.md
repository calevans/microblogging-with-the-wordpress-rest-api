# MicroBlogging with the WordPress REST API

This is a simple demonstration program on how to use the WordPress REST API to create a Micro Blogging Platform.

It is part of ["Using the WordPress REST API"](https://leanpub.com/wordpress_rest_api_i) by Cal Evans